<?php
include_once ("../vendor/autoload.php");

use App\Profile\ProfileData;
$obj = new \App\Question\Question();
$alldata= $obj->getAllQuestion();

$p = new ProfileData();
$mydata = $p->show($_GET['id']);

?>
<!DOCTYPE html>

<html>
<head>
    <title> AmarProshno</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/profiles.css">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="Users.php">
                <img src="../assets/images/red.png" class="titleimage" height="50" width="60">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav ">
                <li><a href="Users.php">Home</a></li>
                <li><a href="Users.php">Question</a></li>
                <li><a href="#">Documentation</a></li>
                <li><a href="#">Tags</a></li>
                <li><a href="#">Users</a></li>
            </ul>
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            </form>

            <a href="Profiles.php">
                <ul class="nav navbar-nav usersprofile ">
                    <li><img src="../assets/images/users/" class=" img-rounded userImage"></li>
                    <li>
                        <sapn class="usersName">
                            <?php echo  $_SESSION['userid']['dname']; ?>
                        </sapn>
                    </li>
                </ul>
            </a>
            <a href="logout.php">
                <ul class="nav navbar-nav logOut">
                    <li>  Log out </li>
                </ul>
            </a>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">


    <div class="row mainContent">
        <div class="col-md-12">
            <div>
                <div class="profileImage">
                    <div class="imgBorder">
                        <img src="../assets/images/users/" name="img" height="160" width="152"  class="imgSize img-rounded ">
                    </div>
                    <div class="editPro">
                         <input type="submit" id="btn" class="btn btn-success btnnnnnn" value="Edit Profile">
                    </div>
                </div>

                <form name="" method="post" action="../Views/Profile/Profile-Info.php">
                    <div id="EditProfile" style="height: auto; margin-left: 15px; border: 1px solid #ddd; background-color: #f4f4f4; width: 840px; float: left; display: none;">
                        <table style="margin-top: 10px;">
                            <tr style="height: 40px;">
                                <td  class="trrr">First Name : </td>
                                <td  class="Edittddd">
                                    <input type="text" name="fname" class="form-control inputBox" size="35">
                                </td>
                            </tr>
                            <tr style="height: 40px;">
                                <td  class="trrr">Last Name : </td>
                                <td  class="Edittddd">
                                    <input type="text" name="lname" class="form-control inputBox" size="35">
                                </td>
                            </tr>
                            <tr style="height: 40px;">
                                <td  class="trrr">Gender : </td>
                                <td  class="Edittddd">
                                    <input type="radio" name="gender" value="Male" size="35">   Male   &nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="Female" size="35">   Female
                                </td>
                            </tr>
                            <tr style="height: 40px;">
                                <td  class="trrr">Date Of Birth : </td>
                                <td  class="Edittddd">
                                    <input type="date" name="dob"  class="form-control inputBox" size="35">
                                </td>
                            </tr>
                            <tr style="height: 40px;">
                                <td  class="trrr">Hobby : </td>
                                <td  class="Edittddd">
                                    <input type="text" name="hobby" class="form-control inputBox" size="35">
                                </td>
                            </tr>
                            <tr style="height: 40px;">
                                <td  class="trrr">Interest : </td>
                                <td  class="Edittddd">
                                    <input type="text" name="interest" class="form-control inputBox" size="35">
                                </td>
                            </tr>
                            <tr style="height: 40px;">
                                <td  class="trrr">Image : </td>
                                <td  class="Edittddd">
                                    <input type="file" name="image" size="35">
                                </td>
                            </tr>
                            <tr >
                                <td colspan="2" style="height: 100px; line-height: 100px;">
                                    <input type="submit"  value="Update" name="update" class="btn btn-success" style="float: right; margin-top: 20px;">
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>

                <div class="profileDetails" id="profileDetails">
                    <table style="margin-top: 10px;">
                        <tr style="height: 40px;">
                            <td  class="trrr">Full Name : </td>
                            <td  class="tddd">
                                <?php if(isset($mydata['first_name'])){
                                    echo $mydata['first_name'];
                                }
                                ?>
                            </td>
                        </tr>
                        <tr style="height: 40px;">
                            <td  class="trrr">Gender : </td>
                            <td  class="tddd">
                                <?php if(isset($mydata['gender'])){
                                    echo $mydata['gender'];
                                }
                                ?>
                            </td>
                        </tr>
                        <tr style="height: 40px;">
                            <td  class="trrr">Date Of Birth : </td>
                            <td  class="tddd">  </td>
                        </tr>
                        <tr style="height: 40px;">
                            <td  class="trrr">Hobby : </td>
                            <td  class="tddd">  </td>
                        </tr>
                        <tr style="height: 40px;">
                            <td  class="trrr">Interest : </td>
                            <td  class="tddd">  </td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-default MainFooter">
    <div class="container-fluid">
        <div class="Footer">
            <p>© All Rights Reserved by Let's Do It,2017.</p>
        </div>
    </div>
</nav>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function(){
        $("#btn").click(function(){
            $("#EditProfile").fadeIn(0);
            $("#profileDetails").fadeOut(0);

        });

    });
</script>
</body>
</html>

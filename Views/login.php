
<!DOCTYPE html>

<html>
    <head>
        <title> AmarProshno</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/login.css">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="../index.php">
                        <img src="../assets/images/red.png" class="titleimage" height="50" width="60">
                        <span class="titleAmar">Amar</span><span class="titleProshno">Proshno</span>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="index.php">Question</a></li>
                        <li><a href="#">Documentation</a></li>
                        <li><a href="#">Tags</a></li>
                        <li><a href="#">Users</a></li>
                    </ul>
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </form>

                    <ul class="nav navbar-nav singUp ">
                        <li><a href="../Views/login.php"><span  class="login">Log In</span></a></li>
                        <li class="btnstyle"><a href="../Views/signUp.php"><button type="button" class="btn btn-primary">Sing Up</button></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logSign">
                        <ul class="select">
                            <li class="active"><a href="#">Log in</a> </li>|
                            <li><a href="../Views/signUp.php">Sign up</a> </li>
                        </ul>
                    </div>
                </div>
            </div><br>

            <div class="row">
                <div class="col-md-12">
                    <div class="CreateTitle">
                        <p >Amar Proshno is part of the Proshno<span class="Exchange">Exchange</span> network of Q&A communities.</p>
                        <?php
                        session_start();
                        if(isset($_SESSION['Message'])){
                            echo $_SESSION['Message'];
                            unset($_SESSION['Message']);
                        }
                        /* session_start();
                         echo $_SESSION['Message'];*/
                        ?>
                    </div>
                </div>
            </div><br>


            <div class="row">
                <div class="col-md-12">
                    <?php
                    if(isset($_SESSION['Message'])){
                        echo $_SESSION['Message'];
                        unset($_SESSION['Message']);
                    } ?>
                    <div class="signUpForm">
                        <form method="post" action="Profile/login-process.php">
                            <div class="SignTable">
                                <br>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="you@example.com">
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Password</label>
                                        <input type="password" name="pwd" class="form-control" id="pwd" placeholder="************">
                                    </div>
                                    <div class="btnn">
                                        <input type="submit" class="btn btn-primary" value="Log in">
                                    </div> <br>
                            </div>
                        </form>
                    </div>
                    <div class="ALreadyLoged">
                        <br><p> Don't have an account? <a href="../Views/signUp.php" class="AlrdyLog">Sign up</a></p> <br>

                    </div>
                </div>
            </div><br><br>

        </div>

        <nav class="navbar navbar-default MainFooter">
            <div class="container-fluid">
                <div class="Footer">
                    <p>© All Rights Reserved by Let's Do It,2017.</p>
                </div>
            </div>
        </nav>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
    </body>
</html>

<?php
include_once ("../vendor/autoload.php");
use App\Question\Question;
$obj = new Question();
$tenQue= $obj->getAllQuestion();

?>
<!DOCTYPE html>

<html>
<head>
    <title> AmarProshno</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/question.css">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="Users.php">
                <img src="../assets/images/red.png" class="titleimage" height="50" width="60">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav ">
                <li><a href="Users.php">Home</a></li>
                <li><a href="Users.php">Question</a></li>
                <li><a href="#">Documentation</a></li>
                <li><a href="#">Tags</a></li>
                <li><a href="#">Users</a></li>
            </ul>
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            </form>

            <a href="Profiles.php">
                <ul class="nav navbar-nav usersprofile ">
                    <li><img src="../assets/images/users/"  class=" img-rounded usersImage"></li>
                    <li>
                        <sapn class="usersN">
                            <?php echo  $_SESSION['userid']['dname']; ?>
                        </sapn>
                    </li>
                </ul>
            </a>
            <a href="logout.php" >
                <ul class="nav navbar-nav logOut">
                    <li>  Log out </li>
                </ul>
            </a>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <div class="row mainContent">
        <div class="col-md-12">
            <div>
                <div class="leftContent">
                    <div class="AskQue">
                        <p>Ask Your Questions</p>
                    </div>
                    <form action="QuestionPage/QuestionStore.php" method="post" >
                    <div class="Answerstable">
                        <p class="InputBox"><input type="text" required="required" placeholder="Please write your question title and write down the your que discription" name="quetitle" class="form-control queInputBox"></p>
                        <div class="textArea">
                            <textarea class="areaBox" name="description"></textarea>
                            <p class="button"><input type="submit" name="submit" class="btn btn-primary btnnn" value="Post"></p>
                        </div>

                    </div>
                    </form>
                </div>


                <div class="rightContent">
                    <div class="askQue">
                        <p class="AskBtn"><a href="login.php"><button type="submit" class="btn btn-primary btnnn" name="submit">Ask Question </button></a></p>
                    </div>

                    <div class="lastTopTenQue">
                        <div class="titleTopten">
                            <p> Top Ten Question</p>
                        </div>
                        <div class="TopQueTen">
                            <?php
                            foreach ($tenQue as $que)
                            {
                                ?>
                                <ul class="TenQue">
                                    <li><a href="#"><?php echo $que['title'] ?></a></li>
                                </ul>
                                <?php
                            }
                            ?>
                        </div>


                    </div>
                    <div class="lastAddTenQue">
                        <div class="titleAddten">
                            <p> Last Added Ten Question</p>
                        </div>
                        <div class="AddQueTen">
                            <?php
                            foreach ($tenQue as $que)
                            {
                                ?>
                                <ul class="AddLastQue">
                                    <li><a href="#"><?php echo $que['title'] ?></a></li>
                                </ul>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="Advertise">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-default MainFooter">
    <div class="container-fluid">
        <div class="Footer">
            <p>© All Rights Reserved by Let's Do It,2017.</p>
        </div>
    </div>
</nav>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
</body>
</html>

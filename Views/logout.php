<?php

//include ("../../vendor/autoload.php");
use App\Utility\Utility;

session_start();

if(!empty( $_SESSION['userid'])){
    unset($_SESSION['userid']);
    $_SESSION['Message'] = "Login First";
    header("location:login.php");
}
?>
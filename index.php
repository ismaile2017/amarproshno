<?php
include_once ("vendor/autoload.php");
$obj = new \App\Question\Question();
$alldata= $obj->getAllQuestion();

?>

<!DOCTYPE html>

<html>
    <head>
        <title>AmarProshno</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="index.php">
                        <img src="assets/images/red.png" class="titleimage" height="50" width="60">
                        <span class="titleAmar">Amar</span><span class="titleProshno">Proshno</span>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="index.php">Question</a></li>
                        <li><a href="#">Documentation</a></li>
                        <li><a href="#">Tags</a></li>
                        <li><a href="#">Users</a></li>
                    </ul>
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </div>
                    </form>

                    <ul class="nav navbar-nav singUp ">
                        <li><a href="Views/login.php"><span  class="login">Log In</span></a></li>
                        <li class="btnstyle"><a href="Views/signUp.php"><button type="button" class="btn btn-primary">Sign Up</button></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header">
                        <p class="JoinTitle">
                            <span>Join the Amar Proshno Community</span>
                        </p>
                        <p class="bottom"></p>
                        <p class="Community">
                            Amar Proshno is a community of 00.0 million programmers, just like you, helping each other.
                            Join them; it only takes a minute:
                        </p></br>
                        <p>
                           <a href="Views/signUp.php"><button type="submit" class="btn btn-primary btn-lg">Sign Up</button></a>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="row mainContent">
                <div class="col-md-12">
                    <div>
                        <div class="leftContent">
                            <div class="topQue">
                                <p>Top Questions</p>
                            </div>
                            <?php
                            foreach ($alldata as $que)
                            {
                            ?>
                            <div class="Quetable">
                                <div class="ImageBox">
                                    <img src="assets/images/users/" class="img-circle usersImage">
                                </div>
                                <div>
                                    <div class="QueBox">
                                        <p  class="QueTitle"><?php echo $que['title'] ?></p>
                                    </div>
                                    <div class="DisBox" style="font-size: 15px;">
                                        <p class="QueDis">
                                            <?php $text=substr($que['description'],0,270); ?>
                                            <?php echo $text;  ?>
                                            <a href="Views/ShowAllUserAnswer.php?id=<?php echo $que['id']  ?>">
                                                <span style="font-size: 14px; border-radius:5px; padding-left: 10px; padding-right: 10px; color: #fff; background-color:#44a669; text-align: center;">
                                                    read more....
                                                </span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                        </div>

                        <div class="rightContent">
                            <div class="askQue">
                                <p class="AskBtn"><a href="Views/login.php"><button type="submit" class="btn btn-primary btnnn" name="submit">Ask Question </button></a></p>
                            </div>

                            <div class="lastTopTenQue">
                                <div class="titleTopten">
                                    <p> Top Ten Question</p>
                                </div>
                                <div class="TopQueTen">
                                    <?php
                                    foreach ($alldata as $que)
                                    {
                                        ?>
                                        <ul class="TenQue">
                                            <li><a href="#"><?php echo $que['title'] ?></a></li>
                                        </ul>
                                        <?php
                                    }
                                    ?>
                                </div>


                            </div>
                            <div class="lastAddTenQue">
                                <div class="titleAddten">
                                    <p> Last Added Ten Question</p>
                                </div>
                                <div class="AddQueTen">
                                    <?php
                                    foreach ($alldata as $que)
                                    {
                                        ?>
                                        <ul class="AddLastQue">
                                            <li><a href="#"><?php echo $que['title'] ?></a></li>
                                        </ul>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="Advertise">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav class="navbar navbar-default MainFooter">
            <div class="container-fluid">
                <div class="Footer">
                    <p>© All Rights Reserved by Let's Do It,2017.</p>
                </div>
            </div>
        </nav>

        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>

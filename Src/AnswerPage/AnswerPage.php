<?php

namespace App\AnswerPage;

use App\Utility\Utility;

use PDO;
@session_start();
class AnswerPage
{

    private $q_id;
    private $description;
    public function setData($data = "")
    {
        $this->q_id = $data['q_id'];

        $this->description = $data['description'];
        return $this;


    }
    public function getAllAnswer(){
        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno', 'root', '');
        $query = 'SELECT * FROM `answer` ORDER BY  	question_id';
        $stmt = $pdo->prepare($query);


        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=amarproshno', 'root', '');
            $query = 'INSERT INTO answer(id,user_id,question_id,description,created_at) VALUES(:myid,:user_id,:q_id,:desc,:created_at)';
            $stmt = $pdo->prepare($query);

            $data = [
                ':myid' => null,
                ':user_id' => 8,
                ':q_id'=>$this->q_id,
                ':desc' =>$this->description,
                ':created_at' => date('Y-m-d h:i:s'),
            ];
            $status = $stmt->execute($data);

            if($status){
                header("location:../../Views/Answer.php?id=$this->q_id");
            }
            else{
                echo"<h1>Opps!Something going wrong.</h1>";
            }


        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
    public function show($id='')
    {

        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno', 'root', '');
        $query = "SELECT * FROM `answer` where question_id=$id ";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }

}

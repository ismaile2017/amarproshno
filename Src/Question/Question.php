<?php
namespace App\Question;
use App\Utility\Utility;
use PDO;
@session_start();
class Question
{

    private $title;
    private $description;
    public function setData($data = "")
    {
        $this->title = $data['quetitle'];
        $this->description = $data['description'];
        return $this;

    }
    public function getAllQuestion(){
        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno', 'root', '');
        $query = 'SELECT * FROM `question` order by id DESC ';
        $stmt = $pdo->prepare($query);


        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=amarproshno', 'root', '');
            $query = 'INSERT INTO question(id,user_id,title,description,created_at) VALUES(:myid,:user_id,:title,:desc,:created_at)';
            $stmt = $pdo->prepare($query);

            $data = [
                ':myid' => null,
                ':user_id' => 8,
                ':title'=>$this->title,
                ':desc' =>$this->description,
                ':created_at' => date('Y-m-d h:i:s'),
            ];
            $status = $stmt->execute($data);

           if($status){
               $_SESSION['Message']= "<span style=' color: #00b008; padding-left: 130px;'>Question Successfully Added.</span>";
                header("location:../../Views/Users.php");
            }
            else{
                echo"<h1>Opps!Something going wrong.</h1>";
            }


        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }


}
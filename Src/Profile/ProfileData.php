<?php
namespace App\Profile;
/*include ("../../vendor/autoload.php");*/
use App\Utility\Utility;
use PDO;
@session_start();
class ProfileData
{
    private $fname;
    private $lname;
    private $gender;
    private $dob;
    private $hobby;
    private $interest;
    private $image;


    public function setData($data = "")
    {
        $this->fname = $data['fname'];
        $this->lname = $data['lname'];
        if(isset($data['gender'])){
            $this->gender = $data['gender'];
        }
        $this->dob = $data['dob'];
        $this->hobby = $data['hobby'];
        $this->interest = $data['interest'];
        $this->image = $data['image'];
       /* echo "<pre>";
        print_r($data);
        dir();*/
        return $this;

    }



    public function show($id="")
    {
        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno', 'root', '');
        $query = "SELECT * FROM `profiles` where user_id=$id ";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }

    public function storeProfileData()
    {

            try {
                $pdo = new PDO('mysql:host=localhost;dbname=amarproshno', 'root', '');
                $query = 'INSERT INTO profiles(id,user_id,first_name,last_name,gender,dob,hobby,interest,image,created_at) VALUES(:myid,:u_id,:fn,:ln,:gender,:dob,:hobby,:interest,:image,:created_at)';
                $stmt = $pdo->prepare($query);

                $data = [
                    ':myid' => null,
                    ':u_id' => $_SESSION['userid']['id'],
                    ':fn' => $this->fname,
                    ':ln' => $this->lname,
                    ':gender' => $this->gender,
                    ':dob' => $this->dob,
                    ':hobby' => $this->hobby,
                    ':interest' => $this->interest,
                    ':image' => $this->image,
                    ':created_at' => date('Y-m-d h:i:s'),

                ];
                $status = $stmt->execute($data);
                if ($status) {
                    //$msg = "<h1>Congratulation! Successfully Registered.</h1>";
                    header("location:../../Views/Profiles.php");
                } else {
                    echo "<h1>Opps!Something going wrong.</h1>";
                    header("location:../../Views/Profiles.php");
                }


            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        }




}